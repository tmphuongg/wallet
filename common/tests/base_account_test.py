from rest_framework.test import APITestCase

from accounts.factories.account import AccountFactory
from accounts.models import Account
from accounts.models import Token


class BaseAccountTest(APITestCase):
    def setUp(self) -> None:
        self.account: Account = AccountFactory()
        self.token = Token.objects.create(user=self.account)
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.token.key}')
