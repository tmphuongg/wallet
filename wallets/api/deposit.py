from rest_framework import serializers
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from wallets.models import Deposit


class DepositSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deposit
        fields = (
            'id',
            'deposited_by',
            'status',
            'deposited_at',
            'amount',
            'reference_id',
        )


class DepositResponseSerializer(serializers.Serializer):
    deposit = DepositSerializer()


class DepositDataSerializer(serializers.Serializer):
    amount = serializers.IntegerField(min_value=0)
    reference_id = serializers.UUIDField()

    def create(self, validated_data):

        return Deposit.objects.create(**validated_data)


class DepositApi(GenericAPIView):
    permission_classes = [IsAuthenticated, ]
    serializer_class = DepositDataSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        deposit = serializer.save(deposited_by=self.request.user)
        request.user.wallet.balance += deposit.amount
        request.user.wallet.save()

        serializer = DepositResponseSerializer({'deposit': deposit})
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)
