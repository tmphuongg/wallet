from django.utils import timezone
from rest_framework import generics
from rest_framework import serializers
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from wallets.models import Wallet


class BaseWalletSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wallet
        fields = (
            'id',
            'owned_by',
            'status',
            'balance',
        )


class DisableWalletSerializer(BaseWalletSerializer):
    class Meta(BaseWalletSerializer.Meta):
        fields = BaseWalletSerializer.Meta.fields + ('disabled_at',)


class DisableWalletDataSerializer(serializers.Serializer):
    is_disabled = serializers.BooleanField()


class EnableWalletSerializer(BaseWalletSerializer):
    class Meta(BaseWalletSerializer.Meta):
        fields = BaseWalletSerializer.Meta.fields + ('enabled_at',)


class WalletResponseSerializer(serializers.Serializer):
    wallet = EnableWalletSerializer()


class DisableWalletResponseSerializer(serializers.Serializer):
    wallet = DisableWalletSerializer()


class WalletApi(generics.GenericAPIView):
    serializer_class = WalletResponseSerializer
    permission_classes = [IsAuthenticated, ]

    def post(self, request, *args, **kwargs):
        wallet, is_created = Wallet.objects.get_or_create(owned_by=request.user,
                                                          defaults={'enabled_at': timezone.now()})
        serializer = self.get_serializer(instance={'wallet': wallet})
        if is_created:
            return Response(status=status.HTTP_201_CREATED, data=serializer.data)
        elif not is_created and wallet.status == 'enabled':
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'error': 'Already enabled'})
        wallet.enable()
        return Response(status=status.HTTP_200_OK)

    def get(self, request):
        wallet = Wallet.objects.get(owned_by=request.user)
        if wallet.status == 'disabled':
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'error': 'Disabled'})
        serializer = self.get_serializer(instance={'wallet': wallet})
        return Response(serializer.data)

    def patch(self, request):
        serializer = DisableWalletDataSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        wallet = Wallet.objects.get(owned_by=request.user)
        wallet.disable()
        serializer = DisableWalletResponseSerializer(instance={'wallet': wallet})
        return Response(serializer.data)
