from rest_framework import serializers
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from wallets.models import Withdrawal


class WithdrawalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Withdrawal
        fields = (
            'id',
            'withdrawn_by',
            'status',
            'withdrawn_at',
            'amount',
            'reference_id',
        )


class WithdrawalResponseSerializer(serializers.Serializer):
    withdrawal = WithdrawalSerializer()


class WithdrawDataSerializer(serializers.Serializer):
    amount = serializers.IntegerField(min_value=0)
    reference_id = serializers.UUIDField()

    def validate(self, attrs):
        account = self.context['request'].user
        if attrs['amount'] > account.wallet.balance:
            raise serializers.ValidationError('Decline due to insufficient funds.')
        return attrs

    def create(self, validated_data):
        return Withdrawal.objects.create(**validated_data)


class WithdrawApi(GenericAPIView):
    permission_classes = [IsAuthenticated, ]
    serializer_class = WithdrawDataSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        deposit = serializer.save(withdrawn_by=self.request.user)
        request.user.wallet.balance -= deposit.amount
        request.user.wallet.save()

        serializer = WithdrawalResponseSerializer({'withdrawal': deposit})
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)
