from django.urls import path

from wallets.api.deposit import DepositApi
from wallets.api.wallet import WalletApi
from wallets.api.withdraw import WithdrawApi

urlpatterns = [
    path('wallet', WalletApi.as_view()),
    path('wallet/deposits', DepositApi.as_view()),
    path('wallet/withdrawals', WithdrawApi.as_view()),

]
