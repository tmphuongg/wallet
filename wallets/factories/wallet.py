import factory
from faker import Faker

from accounts.factories.account import AccountFactory
from wallets.models import Wallet

faker = Faker()


class WalletFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Wallet
    owned_by = factory.SubFactory(AccountFactory)

