from rest_framework import status
from rest_framework.exceptions import ErrorDetail

from common.tests.base_account_test import BaseAccountTest
from wallets.factories.wallet import WalletFactory


class EnableWalletApiTests(BaseAccountTest):
    def _assert_wallet_is_enabled(self, wallet):
        wallet.refresh_from_db()
        self.assertTrue(wallet.is_enabled)

    def test_enable_wallet_successfully(self):
        response = self.client.post('/api/v1/wallet')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.content)
        wallet_data = response.data['wallet']
        self.assertIn('id', wallet_data)
        self.assertEqual(wallet_data['owned_by'], self.account.id)
        self.assertEqual(wallet_data['status'], 'enabled')
        self.assertIsNotNone(wallet_data['enabled_at'])
        self.assertEqual(wallet_data['balance'], 0)

    def test_enable_an_enable_wallet(self):
        wallet = WalletFactory(owned_by=self.account)
        response = self.client.post('/api/v1/wallet')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.content)
        self.assertEqual(response.data, {'error': 'Already enabled'})
        self._assert_wallet_is_enabled(wallet)

    def test_enable_an_disable_wallet(self):
        wallet = WalletFactory(owned_by=self.account)
        wallet.disable()
        response = self.client.post('/api/v1/wallet')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self._assert_wallet_is_enabled(wallet)

