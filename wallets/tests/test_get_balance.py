from rest_framework import status

from common.tests.base_account_test import BaseAccountTest
from wallets.factories.wallet import WalletFactory


class GetWalletBalanceApiTests(BaseAccountTest):
    def test_get_wallet_balance(self):
        wallet = WalletFactory(owned_by=self.account)
        response = self.client.get('/api/v1/wallet')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['wallet']['id'], str(wallet.id))
        self.assertEqual(response.data['wallet']['balance'], wallet.balance)
        self.assertEqual(response.data['wallet']['status'], 'enabled')
        self.assertIn('balance', response.data['wallet'])
        self.assertIn('enabled_at', response.data['wallet'])

    def test_get_disabled_wallet_balance(self):
        wallet = WalletFactory(owned_by=self.account)
        wallet.disable()
        response = self.client.get('/api/v1/wallet')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'error': 'Disabled'})
