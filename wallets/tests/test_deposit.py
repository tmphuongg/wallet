import uuid

from rest_framework import status

from common.tests.base_account_test import BaseAccountTest
from wallets.factories.wallet import WalletFactory


class DepositApiTests(BaseAccountTest):
    def setUp(self) -> None:
        super(DepositApiTests, self).setUp()
        self.wallet = WalletFactory(owned_by=self.account)
        self.assertEqual(self.wallet.balance, 0)

    def test_deposit_successfully(self):
        reference_id = uuid.uuid4()
        response = self.client.post('/api/v1/wallet/deposits',
                                    data={'amount': 100000, 'reference_id': str(reference_id)})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(self.account.deposits.count(), 1)
        deposit_data = response.data['deposit']
        self.assertIn('id', deposit_data)
        self.assertEqual(deposit_data['deposited_by'], self.account.id)
        self.assertIn('status', deposit_data)
        self.assertIn('deposited_at', deposit_data)
        self.assertEqual(deposit_data['amount'], 100000)
        self.assertEqual(deposit_data['reference_id'], str(reference_id))
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.balance, 100000)
