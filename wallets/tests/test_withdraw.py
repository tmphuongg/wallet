import uuid

from rest_framework import status

from common.tests.base_account_test import BaseAccountTest
from wallets.factories.wallet import WalletFactory


class WithdrawApiTests(BaseAccountTest):
    def setUp(self) -> None:
        super(WithdrawApiTests, self).setUp()
        self.wallet = WalletFactory(owned_by=self.account, balance=100000)

    def test_withdraw_successfully(self):
        reference_id = uuid.uuid4()
        response = self.client.post('/api/v1/wallet/withdrawals',
                                    data={'amount': 10000, 'reference_id': str(reference_id)})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(self.account.withdrawals.count(), 1)
        withdrawal_data = response.data['withdrawal']
        self.assertIn('id', withdrawal_data)
        self.assertEqual(withdrawal_data['withdrawn_by'], self.account.id)
        self.assertIn('status', withdrawal_data)
        self.assertIn('withdrawn_at', withdrawal_data)
        self.assertEqual(withdrawal_data['amount'], 10000)
        self.assertEqual(withdrawal_data['reference_id'], str(reference_id))
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.balance, 90000)

    def test_withdraw_failed_due_to_lack_of_money(self):
        response = self.client.post('/api/v1/wallet/withdrawals',
                                    data={'amount': 1000000, 'reference_id': str(uuid.uuid4())})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {'non_field_errors': ['Decline due to insufficient funds.']},
        )
