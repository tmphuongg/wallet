from rest_framework import status
from rest_framework.exceptions import ErrorDetail

from common.tests.base_account_test import BaseAccountTest
from wallets.factories.wallet import WalletFactory


class DisableWalletApiTests(BaseAccountTest):
    def _assert_wallet_is_disabled(self, wallet):
        wallet.refresh_from_db()
        self.assertFalse(wallet.is_enabled)

    def test_disable_wallet_successfully(self):
        wallet = WalletFactory(owned_by=self.account)
        response = self.client.patch('/api/v1/wallet', data={'is_disabled': True})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        wallet_data = response.data['wallet']
        self.assertEqual(wallet_data['id'], str(wallet.id))
        self.assertEqual(wallet_data['owned_by'], self.account.id)
        self.assertEqual(wallet_data['status'], 'disabled')
        self.assertIsNotNone(wallet_data['disabled_at'])
        self.assertIn('balance', wallet_data)
        self._assert_wallet_is_disabled(wallet)
