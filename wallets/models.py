import uuid

from django.db import models
from django.utils import timezone

from common.models import ModelTimestamps

WALLET_STATUS_CHOICES = (
    ('enabled', 'Enabled'),
    ('disabled', 'Disabled'),
)

DEPOSIT_STATUS_CHOICES = (
    ('success', 'Success'),
)


class Wallet(ModelTimestamps):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    owned_by = models.OneToOneField('accounts.Account', on_delete=models.CASCADE)
    status = models.CharField(choices=WALLET_STATUS_CHOICES, default='enabled', max_length=20)
    enabled_at = models.DateTimeField(null=True, blank=True)
    balance = models.IntegerField(default=0)
    disabled_at = models.DateTimeField(null=True, blank=True)

    def enable(self):
        self.status = 'enabled'
        self.enabled_at = timezone.now()
        self.save()

    def disable(self):
        self.status = 'disabled'
        self.disabled_at = timezone.now()
        self.save()

    @property
    def is_enabled(self):
        return self.status == 'enabled'


class Deposit(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    deposited_by = models.ForeignKey('accounts.Account', on_delete=models.CASCADE, related_name='deposits')
    status = models.CharField(choices=DEPOSIT_STATUS_CHOICES, default='success', max_length=20)
    deposited_at = models.DateTimeField(auto_now_add=True)
    amount = models.IntegerField()
    reference_id = models.UUIDField(unique=True)


class Withdrawal(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    withdrawn_by = models.ForeignKey('accounts.Account', on_delete=models.CASCADE, related_name='withdrawals')
    status = models.CharField(choices=DEPOSIT_STATUS_CHOICES, default='success', max_length=20)
    withdrawn_at = models.DateTimeField(auto_now_add=True)
    amount = models.IntegerField()
    reference_id = models.UUIDField(unique=True)
