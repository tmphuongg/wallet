# Mini Wallet

## Setup
### install poetry
``bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -
``
### activate virtual env
```bash
poetry shell
```

## Install new package/ new package in pyproject.toml from other branches

```bash
poetry install
```
## Run test

```bash
poetry run coverage run manage.py test

# report coverage
poetry run coverage report -m
```


## Install new package

```bash
poetry add {package}
```

## Update all packages

```bash
poetry update
```

## Run command

```bash
poetry run python manage.py {command}
```
