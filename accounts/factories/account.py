import factory
from faker import Faker

from accounts.models import Account

faker = Faker()


class AccountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Account
    customer_xid = factory.Faker('uuid4')
