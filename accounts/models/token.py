import uuid

from rest_framework.authtoken.models import Token as _Token


class Token(_Token):

    @classmethod
    def generate_key(cls):
        return str(uuid.uuid4())
