from django.urls import path

from accounts.api.init import InitAccountApi

urlpatterns = [
    path('init', InitAccountApi.as_view()),
]
