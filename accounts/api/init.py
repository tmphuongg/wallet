from rest_framework import generics
from rest_framework import serializers
from rest_framework import status
from accounts.models import Token
from rest_framework.response import Response

from accounts.models import Account


class TokenSerializer(serializers.Serializer):
    token = serializers.UUIDField(source='key')


class AccountSerializer(serializers.Serializer):
    customer_xid = serializers.UUIDField()

    def create(self, validated_data):
        return Account.objects.create(**validated_data)


class InitAccountApi(generics.GenericAPIView):
    serializer_class = AccountSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        account = serializer.save()

        token, _ = Token.objects.get_or_create(user=account)
        response_data = TokenSerializer(instance=token).data
        return Response(status=status.HTTP_201_CREATED, data=response_data)
