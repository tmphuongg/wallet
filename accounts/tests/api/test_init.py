import uuid

from rest_framework import status
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APITestCase


class InitAccountApiTests(APITestCase):
    def setUp(self) -> None:
        self.uuid = uuid.uuid4()

    def test_init_account_successfully(self):
        response = self.client.post('/api/v1/init', data={'customer_xid': self.uuid}, format='json', )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.content)
        self.assertIn('token', response.data)

    def test_init_account_without_data(self):
        response = self.client.post('/api/v1/init')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.content)
        self.assertEqual(response.data,
                         {'customer_xid': [ErrorDetail(string='This field is required.', code='required')]})
