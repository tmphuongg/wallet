from unittest.case import TestCase
from uuid import UUID

from faker import Faker

from accounts.factories.account import AccountFactory
from accounts.models import Token


def is_valid_uuid(val):
    try:
        UUID(str(val))
        return True
    except ValueError:
        return False


faker = Faker()


class TokenModelTests(TestCase):
    def test_save_model(self):
        user = AccountFactory(username=faker.email())
        token = Token(user=user, )
        token.save()
        token.refresh_from_db()
        self.assertTrue(is_valid_uuid(token.key))

    def test_key_not_uuid(self):
        user = AccountFactory(username=faker.email())
        token = Token(user=user, key=faker.sentence())
        token.save()
        token.refresh_from_db()
        self.assertFalse(is_valid_uuid(token.key))
